# Introduction

This project is a POC for all Abidjan hospitals. It involves managing doctors and patients.

> ⚠️ This project is still in POC phase. To analyse the code, go to the [dev branch](https://gitlab.com/zak39/yopsante/-/tree/dev?ref_type=heads).

# Branches

We consider the `master` branch for the production and we create the `dev` branch for the development, so it's a unstable state.

# Requirements

- php 7.2
- node 14.X
- npm 6.X
- laravel 6.

## The PHP extension required

You need to install those extensions :

- php7.2-mysql
- php7.2-xml
- php7.2-curl


## Build the project

### back-end side

```bash
composer i
```

### front-end side

```bash
npm i
```

## Run the docker containers

You can run the project with docker containers via docker-compose.

```bash
docker compose up -d
```

## Database configuration

Be careful, if you use the project with docker, please, specify your IP address in those configurations :

In the `.env` file :

```ini

# ...

DB_CONNECTION=mysql
DB_HOST=<your-ip-address>
DB_PORT=3333
DB_DATABASE=yopsantedb
DB_USERNAME=yop
DB_PASSWORD=yop

# ...
```

In the `./config/database.php` file :

```php
// code

        'mysql' => [
            'driver' => 'mysql',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_HOST', <your-ip-address>),
            'port' => env('DB_PORT', '3333'),

// code
```

## Generate migrations and seeders

To create and fill the database, follow the command line :

```bash
php artisan migrate --seed
```

## Go to dev

Then, you can connect on the dev environment with the `http://<your-ip-address>:8383` from your browser. For example : `http://192.168.1.15:8383`.

> ⚠️ You may need to change the rights of the `storage` folder with the `chmod 777 -R storage/` command.
